<?php

namespace Sample\Facades;

use Sample\Database\Accessor;

class DB extends Facade
{
    /**
     * 取得要包裝的類別名稱
     * 
     * @return string
     */
    public static function getStaticName()
    {
        return Accessor::class;
    }
}
