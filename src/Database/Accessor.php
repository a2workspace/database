<?php

namespace Sample\Database;

use PDO;
use Exception;

class Accessor
{
    /**
     * @var \PDO
     */
    protected static $connection;
    
    /**
     * @var array
     */
    protected static $config = [];

    /**
     * @param  array  $config
     */
    public function config(array $config)
    {
        static::$config = array_merge(static::$config, $config);
    }

    /**
     * 連接資料庫
     * 
     * @param  array  $config
     * @return $this
     * @throws Exception
     */
    public function connect($config = [])
    {
        $config = array_merge(static::$config, $config);
        
        $host = $config['host'] ?? 'localhost';
        $port = $config['port'] ?? '3306';
        $charset = $config['charset'] ?? 'utf8';
        $database = $config['database'] ?? 'php';
        $username = $config['username'] ?? 'user';
        $password = $config['password'] ?? 'user';
        
        try {
            static::$connection = new PDO(
                "mysql:host={$host};port{$port};charset={$charset};dbname={$database}",
                $username,
                $password
            );
        } catch (Exception $exce) {
            throw new Exception("資料庫連線失敗: {$exce->getMessage()}");
        }
        
        return $this;
    }
    
    /**
     * 中斷連接資料庫
     * 
     * @return void
     */
    public function disconnect()
    {
        if (static::$connection instanceof PDO) {
            static::$connection->close();
        }
        
        static::$connection = null;
    }
    
    /**
     * 取得 PDO 實體
     * 
     * @return \PDO
     */
    public function getConnection()
    {
        // 若還未初始，則進行連線
        if (! (static::$connection instanceof PDO)) {
            $this->connect();
        }
        
        return static::$connection;
    }
    
    /**
     * 動態呼叫 PDO 的方法。
     *
     * @param  string  $method
     * @param  array  $parameters
     * @return mixed
     */
    public function __call($method, $parameters)
    {
        return $this->getConnection()->{$method}(...$parameters);
    }
    
    /* =========================================================================
     * = Utils
     * =========================================================================
    **/
    
    /**
     * 取得資料表全部結果
     * 
     * @param  string  $tableName
     * @return array
     */
    public function fetchAll($tableName)
    {
        $query = $this->getConnection()
            ->prepare("SELECT * FROM `$tableName`");

        $query->execute();
        // return $query->debugDumpParams();
        return $query->fetchAll(PDO::FETCH_OBJ);
    }
    
    /**
     * 取得指定 id
     * @param  string $tableName
     * @param  string $id
     * @param  string $fieldName [description]
     * @return array
     */
    public function find($tableName, $id, $fieldName = 'id')
    {
        $query = $this->getConnection()
            ->prepare("SELECT * FROM `$tableName` WHERE `$fieldName`=:id");

        $query->execute(compact('id'));
        return $query->fetch(PDO::FETCH_OBJ);
    }
}
